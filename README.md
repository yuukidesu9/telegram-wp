# Telegram Client for Windows Phone (discontinued)
This repository contains the source code for the Telegram client for Windows Phone 8 and 8.1, now discontinued due to the system's EOL (End of Life). It's a shame they pulled the plug of such a good platform.

The (possible) releases will only install in unlocked devices. So, if your device is still locked, you're kinda behind, because the Store and other services have been discontinued in June 2019.
